mod config;
mod dbus;
mod ui;

use std::cell::Cell;
use std::time::Duration;

use config::Configuration;
use snui::cache::Cache;
use snui::environment::*;
use snui::msg_api::*;
use snui::scene::Position;
use snui::utils::*;
use snui::widgets::*;
use snui::*;
use snui_adwaita::widgets::*;

use ui::*;

use dbus::async_run;

use calloop::{
    channel::{self, sync_channel, SyncSender},
    futures::executor,
    timer::{TimeoutAction, Timer},
};

const DEFAULT_TIMEOUT: i32 = 8000;

#[derive(Debug, Clone, PartialEq)]
pub enum Action {
    Close,
    Clear,
    Reload,
    Restore,
    Reveal(bool),
    Notify(Payload),
}

#[derive(Debug, Clone, PartialEq)]
pub struct Payload {
    pub app_name: String,
    pub summary: String,
    pub body: String,
    pub time: String,
    pub icon: String,
    pub timeout: i32,
    pub urgency: u8,
    pub actions: Vec<String>,
}

pub struct Salut {
    // The amount of notification
    count: usize,
    do_not_disturb: bool,
    sender: SyncSender<Action>,
    app_name: String,
    // The visibility of the client
    visible: bool,
    // The state of the notification list
    expanded: bool,
    // Pin the top notification
    //
    // This prevents the client from being hidden
    pin: bool,
    timeout_id: usize,
    // The notification data.
    //
    // It is taken by the Manager to be inserted in the list.
    payload: Cell<Option<Payload>>,
}

impl Salut {
    fn new(sender: SyncSender<Action>) -> Self {
        Salut {
            count: 0,
            app_name: String::from("salut"),
            do_not_disturb: false,
            sender,
            pin: false,
            visible: false,
            expanded: false,
            timeout_id: 0,
            payload: Cell::new(None),
        }
    }
}

Messages! {
    ApplicationName<&'a str>,
    Visibility<bool>,
    Count<usize>,
    Clear<()>,
    Pin<bool>,
    Busy<bool>,
    Expand<bool>,
    DoNotDisturb<bool>,
    Notification<Option<Payload>>
}

RequestManager! {
    Salut {
        ApplicationName => (this, req) {
            req.from(&this.app_name)
        }
        Visibility => (this, req) {
            req.from(this.visible)
        }
        Pin => (this, req) {
            req.from(this.pin)
        }
        Expand => (this, req) {
            req.from(this.expanded)
        }
        Count => (this, req) {
            req.from(this.count)
        }
        Busy => (this, req) {
            req.from(this.count > 1)
        }
        DoNotDisturb => (this, req) {
            req.from(this.do_not_disturb)
        }
        Notification => (this, req) {
            req.from(this.payload.take())
        }
    }
}

MessageHandler! {
    Salut {
        Clear => (this, _) {
            this.payload.take();
        }
        DoNotDisturb => (this, msg) {
            this.do_not_disturb = msg.get();
        }
        Expand => (this, msg) {
            this.expanded = msg.get();
        }
        Pin => (this, msg) {
            this.pin = msg.get();
        }
        Count => (this, msg) {
            this.count = msg.get();
        }
        ApplicationName => (this, msg) {
            this.app_name.replace_range(0.., msg.get());
        }
        Visibility => (this, msg) {
            this.visible = msg.get();
        }
        Notification => (this, msg) {
            this.payload.set(msg.get());
        }
    }
}

impl FieldParameter for ApplicationName {}
impl FieldParameter for Visibility {}
impl FieldParameter for Count {}
impl FieldParameter for DoNotDisturb {}
impl FieldParameter for Pin {}
impl FieldParameter for Expand {}

fn buttons(actions: &[String]) -> impl Widget<Salut> {
    actions
        .chunks_exact(2)
        .filter_map(|actions| actions[0].ne("default").then_some(actions))
        .map(|actions| {
            let action = actions[0].clone();
            let name = actions[1].clone();
            list::ColumnItem::new(
                Label::from(&name)
                    .clamp()
                    .style("button")
                    .class("rounded")
                    .button(
                        move |this, ctx: &mut UpdateContext<Salut>, env, pointer| match pointer {
                            Pointer::Enter | Pointer::Leave => {
                                env.map(this);
                            }
                            _ => {
                                if pointer.left_button_click().is_some() {
                                    let mut command = action.split(&['(', ')']);
                                    ctx.sender.send(Action::Reveal(false)).unwrap();
                                    if let Some(action) = command.next() {
                                        if let Some(action) =
                                            env.push(Id::from(action)).get::<&str>("actions")
                                        {
                                            let cmd = action.to_owned();
                                            let arg = command.next().unwrap_or_default().to_owned();
                                            std::thread::spawn(move || {
                                                if let Err(e) = std::process::Command::new("sh")
                                                    .arg("-c")
                                                    .arg(format!("{} {}", cmd, arg))
                                                    .output()
                                                {
                                                    eprintln!("{:?}", e)
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        },
                    ),
            )
        })
        .collect::<Column<_, _>>()
        .class("linked")
}

fn content(payload: &Payload) -> impl Widget<Salut> {
    Center::row(
        Label::from(&payload.summary).class("title-4").padding(5.),
        Label::from(&payload.time).class("dim-label").padding(5.),
        LabelCap::new(&payload.body).class("").padding(5.),
    )
}

fn notification(payload: &Payload, cache: &mut Cache, env: &Env) -> Box<dyn Widget<Salut>> {
    let view = if let Ok(source) = cache.source_cache().get(&payload.icon) {
        Column!(GenericIcon::from(source)
            .with_fixed_size(90., 90.)
            .class("icon")
            .padding_right(5.))
    } else {
        Column::new()
    }
    .with_child(content(payload).clamp().anchor(START, CENTER));
    if env
        .push(Id::from("buttons"))
        .get("notification")
        .unwrap_or_default()
        && payload.actions.iter().any(|s| s.as_str() != "default")
    {
        Box::new(Row!(
            view.padding_bottom(5.),
            buttons(payload.actions.as_slice()).with_fixed_height(45.)
        ))
    } else {
        Box::new(view)
    }
}

fn popup() -> impl Widget<Salut> {
    AdwPopupList::new(
        ["Clear", "Hide", "Reload", "Exit"],
        |name, ctx: &mut UpdateContext<Salut>| match name {
            "Clear" => ctx.sender.send(Action::Clear).unwrap(),
            "Exit" => ctx.sender.send(Action::Close).unwrap(),
            "Reload" => ctx.sender.send(Action::Reload).unwrap(),
            "Hide" => ctx.sender.send(Action::Reveal(false)).unwrap(),
            _ => {}
        },
    )
    .with_fixed_width(120.)
    .button(|_, ctx, _, p| {
        if let Pointer::Enter = p {
            ctx.post(&Pin, true)
        }
    })
}

fn header() -> impl Widget<Salut> + Themeable + Style {
    use std::ops::DerefMut;
    Center::column(
        AdwToggle::new::<DoNotDisturb, _>(DoNotDisturb)
            .padding(1.)
            .clamp()
            .anchor(START, CENTER),
        Column!(
            Circle::default()
                .class("dim")
                .with_max_size(0., 0.)
                .padding(0.)
                .activate(Busy, |this, state, _ctx: &mut UpdateContext<Salut>, env| {
                    let diameter = state.then_some(5.).unwrap_or_default();
                    this.set_padding_right(diameter);
                    this.set_size(diameter, diameter);
                    env.map(this.deref_mut());
                })
                .flex(Orientation::Vertical),
            DynLabel::new(ApplicationName)
                .class("dim-label")
                .flex(Orientation::Vertical),
        )
        .flex(Orientation::Vertical)
        .style("button")
        .class("flat")
        .class("rounded")
        .button(move |this, ctx, env, pointer| match pointer {
            Pointer::Enter | Pointer::Leave => {
                env.map(this);
            }
            _ => {
                if pointer.left_button_click().is_some() {
                    let action = if env.push(Id::from("single")).get("mode").unwrap_or_default() {
                        if ctx.expanded || ctx.count == 1 {
                            ctx.post(&Expand, false);
                            Action::Reveal(false)
                        } else {
                            Action::Restore
                        }
                    } else {
                        Action::Clear
                    };
                    ctx.sender.send(action).unwrap();
                } else if pointer.middle_button_click().is_some()
                    && env
                        .push(Id::from("testing"))
                        .get("experimental")
                        .unwrap_or_default()
                {
                    ctx.sender
                        .send(Action::Notify(Payload {
                            app_name: String::from("snui"),
                            summary: String::from("test notification"),
                            time: String::from("10:00"),
                            body: String::from("standard notification body"),
                            actions: vec![
                                String::from("app.launch-default-for-file(\"/hello/world\")"),
                                String::from("Open File"),
                                String::from("app.launch-default-for-file(\"/hello/world\")"),
                                String::from("Close File"),
                            ],
                            urgency: 1,
                            icon: String::new(),
                            timeout: -1,
                        }))
                        .unwrap();
                } else if pointer.right_button_click().is_some() {
                    ctx.sender.send(Action::Reveal(false)).unwrap();
                }
            }
        })
        .activate(Visibility, |this, vis, ctx, env| {
            if !vis {
                this.event(
                    ctx,
                    MouseEvent::new(Position::default(), Pointer::Leave),
                    env,
                );
                ctx.post(&Expand, false)
            }
        }),
        Canvas::from(icons::Menu::default())
            .class("icon")
            .style("flat")
            .class("button")
            .class("pill")
            .activate(
                Visibility,
                |this, vis, _ctx: &mut UpdateContext<Salut>, env| {
                    if !vis {
                        env.map(this);
                    }
                },
            )
            .button(move |this, ctx, env, pointer| match pointer {
                Pointer::Enter | Pointer::Leave => {
                    env.map(this);
                }
                _ => {
                    if let Some(serial) = pointer.left_button_click() {
                        ctx.create_popup(move |ctx| {
                            let mut popup = popup();
                            let size = popup.layout(ctx, &BoxConstraints::default(), env);
                            PopupBuilder::default()
                                .serial(serial)
                                .size(size)
                                .anchor(END, START)
                                .offset(Position::new(-size.width / 4., size.height + 20.))
                                .finish(popup)
                        });
                    } else if pointer.left_button_release().is_some() {
                        ctx.post(&Pin, true)
                    }
                }
            })
            .flex(Orientation::Vertical),
    )
    .style("toolbar")
}

fn manager() -> impl Widget<Salut> {
    Manager::new(notification).padding(10.).class("compact")
}

fn window() -> impl Widget<Salut> {
    let mut id = 0;
    Window::new(
        header(),
        Row!(
            Rectangle::default()
                .class("border")
                .with_fixed_height(0.)
                .activate(Visibility, |this, visible, _, _| {
                    this.set_height(visible.then_some(1.).unwrap_or_default());
                }),
            Body::from(manager())
        ),
    )
    .class("background")
    .with_fixed_size(400., 0.)
    .class("window")
    .button(move |_, ctx, env, p| match p {
        Pointer::Enter => {
            id = ctx.timeout_id;
            ctx.post(&Pin, true);
        }
        Pointer::Leave => {
            if env
                .push(Id::from("auto-hide"))
                .get("window")
                .unwrap_or_default()
                && id == ctx.timeout_id
            {
                if ctx.window().children() == 0 {
                    ctx.post(&Visibility, true);
                }
                ctx.post(&Pin, false);
            }
        }
        _ => {}
    })
}

fn main() {
    use snui_wayland::*;

    let (sender, receiver) = sync_channel(1);

    let mut salut = Salut::new(sender.clone());

    let config = Configuration::new();

    let (anchor_x, anchor_y) = config
        .get_key("window", "anchor")
        .map(|key| match key {
            "TOP" | "top" => (CENTER, START),
            "RIGHT" | "right" => (END, CENTER),
            "BOTTOM" | "bottom" => (CENTER, END),
            "LEFT" | "left" => (START, CENTER),
            "TOP-LEFT" | "top-left" => (START, START),
            "TOP-RIGHT" | "top-right" => (END, START),
            "BOTTOM-LEFT" | "bottom-left" => (START, END),
            "BOTTOM-RIGHT" | "bottom-right" => (END, END),
            _ => panic!("window.anchor: invalid key"),
        })
        .unwrap_or((CENTER, START));

    let transition = config
        .get_key("window", "transition")
        .map(|key| match key {
            "SlideTop" | "slidetop" => Transition::SlideTop,
            "SlideLeft" | "slideleft" => Transition::SlideLeft,
            "SlideBottom" | "slidebottom" => Transition::SlideBottom,
            "SlideRight" | "slideright" => Transition::SlideRight,
            _ => panic!("window.transition: invalid key"),
        })
        .unwrap_or(Transition::SlideTop);

    let duration = config
        .get_key("window", "animation-duration")
        .unwrap_or("500")
        .parse::<u32>()
        .expect("window.animation-duration: invalid key");

    let output = config
        .get_key("window", "output")
        .unwrap_or_default()
        .to_owned();

    salut.do_not_disturb = config
        .get_key("mode", "do-not-disturb")
        .unwrap_or("false")
        .parse::<bool>()
        .expect("mode.do-not-disturb: invalid key");

    let mut id = None;

    let (mut client, mut event_loop) = WaylandClient::new_with_cb(salut, |client, qh| {
        client.set_environment(config);
        client.set_app_id("org.snui.snakedye.salut");
        let mut view = client.create_widget(
            // window()
            //     .padding(10.)
            //     .class("window-margin")
            Revealer::<_, _, Sinus, _>::new(Visibility, window().padding(10.).class("window"))
                .transition(transition)
                .duration(duration)
                .with_fixed_size(1., 1.),
            LayerSurfaceBuilder::default()
                .output(output)
                .anchor(anchor_x, anchor_y),
            qh,
        );
        view.set_min_size(300, 10);
        id = Some(view.id());
    })
    .unwrap();

    let (exec, sched) = executor().unwrap();

    let loop_handle = event_loop.handle();

    loop_handle
        .insert_source(exec, |_, _, _| {})
        .expect("Failed to insert the executor!");

    event_loop
        .handle()
        .insert_source(receiver, move |event, _, client| {
            if let channel::Event::Msg(action) = event {
                match action {
                    Action::Notify(payload) => {
                        let timeout = payload
                            .timeout
                            .is_negative()
                            .then(|| {
                                Env::new(client.environment())
                                    .push(Id::from("notification"))
                                    .get("timeout")
                                    .unwrap_or(DEFAULT_TIMEOUT)
                            })
                            .unwrap_or(payload.timeout);
                        let salut = client.data();
                        if !salut.do_not_disturb {
                            salut.visible = true;
                            salut.timeout_id += 1;
                            let id = id.clone().unwrap();
                            let timeout_id = salut.timeout_id;
                            if payload.urgency <= 1 && !salut.pin {
                                loop_handle
                                    .insert_source(
                                        Timer::from_duration(Duration::from_millis(timeout as u64)),
                                        move |_, _, client| {
                                            let has_children = client
                                                .view_handle(&id)
                                                .map(|view| view.children() > 0)
                                                .unwrap_or_default();
                                            let salut = client.data();
                                            if timeout_id == salut.timeout_id
                                                && salut.visible
                                                && !salut.pin
                                                && !has_children
                                            {
                                                salut.visible = false;
                                                client.event(Event::Message("restore")).unwrap();
                                            }
                                            TimeoutAction::Drop
                                        },
                                    )
                                    .expect("Failed to insert a timer!");
                            }
                        }
                        salut.payload.set(Some(payload));
                        client.focus(id.clone().unwrap());
                        client.event(Event::Message("notification")).unwrap();
                    }
                    Action::Close => {
                        if let Some(mut view) = client.view_handle(id.as_ref().unwrap()) {
                            view.close();
                        }
                        client.event(Event::Configure).unwrap();
                    }
                    Action::Reload => {
                        client.set_environment(Configuration::new());
                        client.event(snui::environment::STYLESHEET_SIGNAL).unwrap();
                    }
                    Action::Clear => {
                        client.data().app_name.replace_range(0.., "salut");
                        client.event(Event::Message("clear")).unwrap();
                    }
                    Action::Restore => {
                        client.data().expanded = true;
                        client.event(Event::Message("restore")).unwrap();
                    }
                    Action::Reveal(state) => {
                        let salut = client.data();
                        if salut.count > 0 || !state {
                            salut.visible = state;
                            client.event(Event::Message("restore")).unwrap();
                        }
                    }
                }
            }
        })
        .expect("Failed to insert the receiver!");

    sched.schedule(async_run(sender)).unwrap();
    event_loop.run(None, &mut client, |_| {}).unwrap();
}
