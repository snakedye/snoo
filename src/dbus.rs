use super::{Action, Payload};
use calloop::channel::SyncSender;
use chrono::Local;
use std::collections::HashMap;
use zbus::zvariant::Value;
use zbus::{dbus_interface, Connection};

const CAPABILITIES: [&str; 6] = [
    "icons",
    "actions",
    "body",
    "body-images",
    "action-icons",
    "persistence",
];

const SERVER_INFORMATION: (&str, &str, &str, &str) = ("salut", "snui.org", "0.2.1", "1.0");

pub struct NotificationServer {
    sender: SyncSender<Action>,
}

impl NotificationServer {
    pub fn new(sender: SyncSender<Action>) -> Self {
        Self { sender }
    }
}

const NAME: &str = "org.freedesktop.Notifications";
const PATH: &str = "/org/freedesktop/Notifications";

#[dbus_interface(name = "org.freedesktop.Notifications")]
impl NotificationServer {
    fn notify(
        &self,
        app_name: String,
        _replaces_id: u32,
        app_icon: String,
        summary: String,
        body: String,
        actions: Vec<String>,
        hints: HashMap<String, Value<'_>>,
        expire_timeout: i32,
    ) -> i32 {
        let dt = Local::now();
        let time = dt.time().format("%H:%M").to_string();
        let payload = Payload {
            app_name,
            summary,
            time,
            body,
            actions,
            urgency: hints
                .get("urgency")
                .and_then(|v| v.clone().downcast())
                .unwrap_or_default(),
            icon: app_icon,
            timeout: expire_timeout,
        };
        self.sender
            .send(Action::Notify(payload))
            .map(|_| 0)
            .unwrap_or(-1)
    }

    fn restore(&self) {
        if let Err(e) = self.sender.send(Action::Reveal(true)) {
            eprintln!("{:?}", e);
        }
    }

    fn reload(&self) {
        if let Err(e) = self.sender.send(Action::Reload) {
            eprintln!("{:?}", e);
        }
    }

    fn exit(&self) {
        if let Err(e) = self.sender.send(Action::Close) {
            eprintln!("{:?}", e);
        }
    }

    fn get_capabilities(&self) -> &[&str] {
        &CAPABILITIES
    }
    fn get_server_information(&self) -> (&str, &str, &str, &str) {
        SERVER_INFORMATION
    }
}

pub async fn async_run(sender: SyncSender<Action>) -> zbus::Result<()> {
    let connection = Connection::session().await?;

    let server = NotificationServer::new(sender);

    connection.object_server().at(PATH, server).await?;

    connection.request_name(NAME).await?;

    Ok(())
}
