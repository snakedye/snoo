mod label;
mod notification;

pub use label::LabelCap;
pub use notification::Manager;

use super::*;
use snui::*;

/// The body of the application.
pub struct Body<W> {
    inner: W,
    visible: bool,
}

impl<W: Widget<Salut>> Widget<Salut> for Body<W> {
    fn draw_scene(&mut self, scene: scene::Scene, env: &Env) {
        if self.visible {
            self.inner.draw_scene(scene, env)
        }
    }
    fn event(&mut self, ctx: &mut UpdateContext<Salut>, event: Event, env: &Env) -> Damage {
        self.inner.event(ctx, event, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<Salut>, env: &Env) -> Damage {
        ctx.prepare();
        self.visible = ctx.count > 0;
        self.inner.update(ctx, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<Salut>, bc: &BoxConstraints, env: &Env) -> Size {
        // If there's no notification, the body will take no size and only the header will be shown.
        self.visible
            .then(|| self.inner.layout(ctx, bc, env))
            .unwrap_or_default()
    }
}

impl<W> From<W> for Body<W> {
    fn from(inner: W) -> Self {
        Self {
            visible: false,
            inner,
        }
    }
}
