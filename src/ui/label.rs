use std::ops::DerefMut;

use snui::environment::*;
use snui::widgets::Label;
use snui::*;

/// An expandable [`Label`].
///
/// By default it only takes one line but can be expanded to reveal its full content.
pub struct LabelCap {
    label: Label,
    expand: bool,
    content: String,
    len: Option<usize>,
}

impl Themeable for LabelCap {
    const NAME: &'static str = Label::NAME;
    fn theme(&mut self, id: utils::List<Id>, env: &dyn Environment) {
        self.label.theme(id, env);
    }
}

impl Guarded for LabelCap {
    fn restore(&mut self) {}
    fn touched(&self) -> bool {
        self.label.touched()
    }
}

impl<T> Widget<T> for LabelCap {
    fn draw_scene(&mut self, scene: scene::Scene, env: &Env) {
        Widget::<T>::draw_scene(&mut self.label, scene, env);
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        match event {
            Event::Focus(focus) => {
                if focus.is_pointer_focus() {
                    self.expand = !self.expand;
                    if !self.expand {
                        self.shorten();
                    } else {
                        self.label.edit(self.content.as_str());
                    }
                    return Damage::Partial;
                }
            }
            Event::Pointer(MouseEvent { pointer, .. }) => {
                if pointer.middle_button_click().is_some() {
                    if !self.expand {
                        self.expand = true;
                    } else {
                        self.expand = false;
                    }
                    return Damage::Partial;
                } else {
                    return Damage::None;
                }
            }
            _ => {}
        }
        Widget::<T>::event(&mut self.label, ctx, event, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        Widget::<T>::update(&mut self.label, ctx, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        if self.len.is_none() {
            self.label.deref_mut();
            Widget::<T>::layout(&mut self.label, ctx, bc, env);
            if let Some(lines) = self.label.text_layout().lines() {
                let len = lines[0].glyph_end - lines[0].glyph_start;
                if len > 0 {
                    self.len = Some(len + 1);
                    self.expand = false;
                    self.shorten();
                }
            }
        }
        Widget::<T>::layout(&mut self.label, ctx, bc, env)
    }
}

impl LabelCap {
    pub fn new(s: &str) -> Self {
        Self {
            content: s.to_owned(),
            expand: false,
            len: None,
            label: Label::from(s),
        }
    }
    fn shorten(&mut self) {
        let g_len = self.len.unwrap_or_default();
        if self.label.chars().count() > g_len {
            let len: usize = self
                .label
                .char_indices()
                .map_while(|(i, c)| (i < g_len).then(|| c.len_utf8()))
                .sum();
            self.label.edit(&self.content[0..len - 1.min(len)]);
            self.label.push_str("..");
        }
    }
}
