use crate::*;
use snui::cache::Cache;
use snui::environment::*;
use snui::widgets::{Positioner, RowIter, Stylesheet, WidgetStyle};
use snui::*;

type InnerItem<T, W> = Stylesheet<T, Stylesheet<T, Stylesheet<T, WidgetStyle<T, W>>>>;

/// A widget wrapping a notification.
pub struct Item<W: Widget<Salut>> {
    done: bool,
    payload: Payload,
    inner: InnerItem<Salut, W>,
}

impl<W: Widget<Salut>> Widget<Salut> for Item<W> {
    fn draw_scene(&mut self, scene: scene::Scene, env: &Env) {
        self.inner.draw_scene(scene, env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<Salut>, event: Event, env: &Env) -> Damage {
        let env = env.push(Id::from("button"));
        match event {
            Event::Pointer(MouseEvent { pointer, .. }) => match pointer {
                Pointer::Enter => {
                    if ctx.get(&Visibility).get() {
                        env.set_class("hover").map(&mut self.inner);
                        ctx.post(&ApplicationName, self.payload.app_name.as_str());
                    }
                }
                Pointer::Leave => {
                    env.map(&mut self.inner);
                }
                _ => {
                    if pointer.middle_button_click().is_some() {
                        return self
                            .inner
                            .event(ctx, Event::Focus(Focus::pointer_focus()), &env);
                    } else if pointer.left_button_click().is_some() {
                        ctx.update();
                        self.done = true;
                        if let Some(action) = self
                            .payload
                            .actions
                            .chunks_exact(2)
                            .find(|a| a[0].as_str() == "default")
                        {
                            let mut command = action[1].split(&['(', ')']);
                            if let Some(action) = command.next() {
                                if let Some(action) =
                                    env.push(Id::from(action)).get::<&str>("actions")
                                {
                                    let cmd = action.to_owned();
                                    let arg = command.next().unwrap_or_default().to_owned();
                                    std::thread::spawn(move || {
                                        if let Err(e) = std::process::Command::new("sh")
                                            .arg("-c")
                                            .arg(format!("{} {}", cmd, arg))
                                            .output()
                                        {
                                            eprintln!("{:?}", e)
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
            },
            Event::Focus(Focus { state, .. }) => {
                if !state {
                    env.map(&mut self.inner);
                }
            }
            Event::Message(_) => ctx.prepare(),
            _ => {}
        }
        self.inner.event(ctx, event, &env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<Salut>, env: &Env) -> Damage {
        let env = env.push(Id::from("button"));
        if !ctx.get(&Visibility).get() {
            env.map(&mut self.inner);
            self.payload.urgency = 1;
        }
        self.inner.update(ctx, &env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<Salut>, bc: &BoxConstraints, env: &Env) -> Size {
        self.inner.layout(ctx, bc, env)
    }
}

/// The notification manager.
///
/// It insert new notifications in a list and removes them when they are done.
pub struct Manager<F, W>
where
    W: Widget<Salut>,
    F: FnMut(&Payload, &mut Cache, &Env) -> W,
{
    len: usize,
    builder: F,
    children: Vec<Positioner<Proxy<Salut, Item<W>>>>,
}

impl<W, F> Manager<F, W>
where
    W: Widget<Salut>,
    F: FnMut(&Payload, &mut Cache, &Env) -> W,
{
    pub fn new(builder: F) -> Self {
        Self {
            len: 1,
            builder,
            children: Vec::new(),
        }
    }
    pub fn notify(&mut self, ctx: &mut UpdateContext<Salut>, env: &Env) {
        if let Some(payload) = ctx.get(&Notification).get() {
            if env
                .push(Id::from("debug"))
                .get("experimental")
                .unwrap_or_default()
            {
                println!(
                    "\nNOTIFICATION ------------------------------\n{:#?}\n",
                    payload
                );
            }
            let mut notification = (self.builder)(&payload, &mut ctx.cache, env)
                .style("button")
                .class("rounded")
                .class("flat");
            notification.layout(&mut ctx.layout(), &BoxConstraints::default(), env);
            ctx.post(&ApplicationName, payload.app_name.as_str());
            ctx.window().set_title(payload.summary.clone());
            ctx.post(&Pin, payload.urgency > 1);
            let item = Positioner::new(Proxy::new(Item {
                payload,
                done: false,
                inner: notification,
            }));
            if self
                .children
                .last()
                .map(|i| i.payload.urgency)
                .unwrap_or_default()
                <= item.payload.urgency
            {
                self.children.push(item);
            } else {
                let i = self.children.len().max(1) - 1;
                self.children.insert(i, item);
            }
            ctx.post(&Count, self.children.len());
            if env.push(Id::from("single")).get("mode").unwrap_or_default() && !ctx.expanded {
                self.len = 1;
            } else {
                self.len = ctx.count;
            }
        }
    }
}

impl<F, W> Widget<Salut> for Manager<F, W>
where
    W: Widget<Salut>,
    F: FnMut(&Payload, &mut Cache, &Env) -> W,
{
    fn draw_scene(&mut self, mut scene: scene::Scene, env: &Env) {
        for (widget, _) in self.children.iter_mut().rev().zip(0..self.len) {
            widget.draw_scene(scene.borrow(), env)
        }
    }
    fn event(&mut self, ctx: &mut UpdateContext<Salut>, event: Event, env: &Env) -> Damage {
        if let Event::Message(msg) = event {
            match msg {
                "notification" => {
                    self.notify(ctx, env);
                }
                "restore" => {
                    ctx.update();
                    if ctx.visible {
                        self.len = self.children.len();
                    }
                }
                STYLESHEET_MESSAGE => {
                    return self
                        .children
                        .iter_mut()
                        .map(|w| w.event(ctx, event, env))
                        .max()
                        .unwrap_or_default();
                }
                "clear" => {
                    ctx.post(&Count, 0);
                    self.children.clear();
                    return Damage::Partial;
                }
                _ => {}
            }
        }
        let mut damage = Damage::None;
        let mut i = self.children.len();
        self.children.retain_mut(|widget| {
            if i <= self.len {
                damage = damage.max(widget.event(ctx, event, env));
                if widget.done {
                    self.len -= 1;
                    damage = damage.max(Damage::Partial)
                }
            }
            i -= 1;
            !widget.done && i < 8
        });
        if self.children.len() == 0 {
            ctx.post(&Visibility, false);
        }
        ctx.post(
            &Count,
            (self.len != 0)
                .then_some(self.children.len())
                .unwrap_or_default(),
        );
        damage
    }
    fn update(&mut self, ctx: &mut UpdateContext<Salut>, env: &Env) -> Damage {
        if self.len == 0 {
            ctx.post(&Count, self.children.len());
        }
        self.children
            .iter_mut()
            .rev()
            .map(|view| view.update(ctx, env))
            .max()
            .unwrap_or_default()
    }
    fn layout(&mut self, ctx: &mut LayoutContext<Salut>, bc: &BoxConstraints, env: &Env) -> Size {
        RowIter::new(self.children.iter_mut().rev(), ctx, bc, env)
            .zip(0..self.len)
            .map(|(region, _)| region)
            .reduce(|acc, region| acc.merge(&region))
            .unwrap_or_default()
            .size()
    }
}
