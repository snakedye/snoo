use ini::ini;
use snui::environment::{Environment, Id, Value};
use snui::utils::*;
use snui_adwaita::adwaita::AdwaitaTheme;
use snui_adwaita::load_theme;
use std::collections::HashMap;
use std::env::var;

/// The configuration of salut.
pub struct Configuration {
    pub theme: AdwaitaTheme,
    pub config: HashMap<String, HashMap<String, Option<String>>>,
}

pub fn last_class<'a>(list: &'a List<Id>) -> Option<&'a str> {
    list.iter().filter_map(|i| i.class()).next()
}

fn get_key<'a>(
    map: &'a HashMap<String, HashMap<String, Option<String>>>,
    group: &str,
    key: &str,
) -> Option<&'a str> {
    map.get(group)
        .and_then(|group| group.get(key).and_then(|key| key.as_ref()))
        .map(|key| key.as_str())
}

pub fn get_float<'a>(
    map: &'a HashMap<String, HashMap<String, Option<String>>>,
    group: &str,
    key: &str,
) -> Option<Value<'a>> {
    get_key(map, group, key)?
        .parse::<f32>()
        .ok()
        .map(Value::from)
}

impl Environment for Configuration {
    fn get(&self, list: List<Id>, property: &str) -> Option<Value> {
        self.theme.get(list, property).or_else(|| match property {
            "padding" => last_class(&list).and_then(|class| match class {
                "window" => self
                    .get_key("window", "margin")?
                    .parse::<f32>()
                    .ok()
                    .map(Value::from),
                _ => None,
            }),
            "width" => last_class(&list).and_then(|class| match class {
                "icon" => get_float(&self.config, "notification", "icon-size"),
                "header" | "window" => get_key(&self.config, "window", "max-width")?
                    .parse::<f32>()
                    .ok()
                    .map(Value::from),
                _ => None,
            }),
            "height" => last_class(&list).and_then(|class| match class {
                "icon" => get_float(&self.config, "notification", "icon-size"),
                _ => None,
            }),
            "actions" | "action" | "experimental" | "debug" | "mode" | "features"
            | "notification" => self.get_key(property, list.item()).map(Value::from),
            _ => None,
        })
    }
}

impl Configuration {
    pub fn get_key<'a>(&'a self, group: &str, key: &str) -> Option<&'a str> {
        get_key(&self.config, group, key)
    }
}

impl Configuration {
    pub fn new() -> Self {
        let mut config = var("XDG_CONFIG_HOME").unwrap_or({
            let mut home = var("HOME").unwrap_or_else(|_| String::from("~/"));
            home.push_str("/.config");
            home
        });
        config.push_str("/salut/config.ini");
        let map = ini!(config.as_str());
        let theme = get_key(&map, "style", "preference")
            .map(|pref| match pref {
                "light" => AdwaitaTheme::light(),
                "dark" => AdwaitaTheme::dark(),
                "system" => {
                    load_theme(get_key(&map, "style", "path")).unwrap_or(AdwaitaTheme::dark())
                }
                _ => AdwaitaTheme::dark(),
            })
            .unwrap_or(AdwaitaTheme::dark());
        Self { theme, config: map }
    }
}

use std::ops::Deref;

impl Deref for Configuration {
    type Target = HashMap<String, HashMap<String, Option<String>>>;
    fn deref(&self) -> &Self::Target {
        &self.config
    }
}
